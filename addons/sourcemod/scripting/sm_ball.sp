#define VERSION			"1.3.23"
public Plugin myinfo = 
{
	name		= "Simple Soccer Ball",
	author		= "mottzi & ShaRen",
	description	= "Simple Ball for CS:GO",
	version		= VERSION,
	url			= "https://forums.alliedmods.net/showthread.php?p=2423345"
}

#include <sourcemod>
#include <sdkhooks>
#include <emitsoundany>

stock void debugMessage(const char[] message, any ...)
{
	char szMessage[256], szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/debug_ball.txt");
	VFormat(szMessage, sizeof(szMessage), message, 2);
	LogToFile(szPath, szMessage);
}
#define dbgMsg(%0) debugMessage(%0)

// *** 
// only modify if you know what you're doing
#define BALL_ENTITY_NAME			"simpleball"
//#define BALL_CFG_FILE				"configs/ballspawns.cfg"
#define BALL_PLAYER_DISTANCE		55.0
#define BALL_KICK_DISTANCE			55.0			// во время броска на каком расстоянии от игрока появляеся мяч
#define BALL_KICK_POWER				100.0			// сила отскока
#define BALL_HOLD_HEIGHT			15				// высона на которой игрок держит мяч
#define BALL_KICK_HEIGHT_ADDITION	25
#define BALL_RADIUS					16.0
#define BALL_AUTO_RESPAWN			35.0			// время
// thanks.
// *** 

#define FSOLID_NOT_SOLID			0x0004
#define FSOLID_TRIGGER				0x0008
#define IsClientValid(%0)			(1 <= %0 <= MaxClients)

int g_Ball;
int g_BallHolder;

float g_fUseHoldTime[MAXPLAYERS+1];
float g_fBallSpawnOrigin[3];
bool g_BallSpawnExists;

Handle g_TimerRespawn = INVALID_HANDLE;

void InitializeVariables()
{
	g_BallHolder = 0;
	g_BallSpawnExists = false;
	g_TimerRespawn = INVALID_HANDLE;
}

public OnPluginStart()
{
	RegConsoleCmd("ball", CommandBallMenu);
	
	AddNormalSoundHook(Event_SoundPlayed);
	HookEvent("round_start", EventRoundStart);
	HookEvent("player_death", EventPlayerDeath, EventHookMode_Pre);
}

public Action CommandBallMenu(int client, int args)
{
	if (IsClientInGame(client) && GetClientTeam(client) == 3 && IsPlayerAlive(client))
		BallMenu(client);
}

void BallMenu(int client)
{
	Menu menu = new Menu(BallMenuHandler);
	
	menu.SetTitle("[МЯЧ] Меню");	
	menu.AddItem("", "Убрать мяч");
	menu.AddItem("", "Создать мяч");
	menu.AddItem("", "Пересоздать мяч");
		
	menu.Display(client, MENU_TIME_FOREVER);
}

public int BallMenuHandler(Menu menu, MenuAction action, int iClient, int param2)
{
	switch(action) {
		case MenuAction_Select:
			if (IsClientInGame(iClient) && GetClientTeam(iClient) == 3 && IsPlayerAlive(iClient)) {
				switch(param2) {
					// remove ball
					case 0: 
						if(g_BallSpawnExists) {
							Kill_g_iBall();
							InitializeVariables();
							PrintToChat(iClient, "[SM] Мяч удален.");
							dbgMsg("[SM] Мяч удален.");
						}
					case 1: { // add ball
						Kill_g_iBall();
						InitializeVariables();
						GetPlayerEyeViewPoint(iClient, g_fBallSpawnOrigin);
						g_fBallSpawnOrigin[2] += 20.0;
						PrintToServer("%f %f %f", g_fBallSpawnOrigin[0], g_fBallSpawnOrigin[1], g_fBallSpawnOrigin[2]);
						
						g_BallSpawnExists = true;
						
						RespawnBall();
						
						PrintToChat(iClient, "[SM] Мяч добавлен.");
						dbgMsg("[SM] Мяч добавлен.");
					} case 2:
						if(g_BallSpawnExists) {
							RespawnBall();
							PrintToChat(iClient, "[SM] Мяч обновлен.");
							dbgMsg("[SM] Мяч обновлен.");
						}
				}
				BallMenu(iClient);
			} else delete menu;
		case MenuAction_End: delete menu;
	}
}

public OnMapStart()
{
	dbgMsg("[SM] OnMapStart------------------------------------------------------");
	InitializeVariables();

	PrecacheSoundAny("knastjunkies/bounce.mp3");
	AddFileToDownloadsTable("sound/knastjunkies/bounce.mp3");
	PrecacheSoundAny("knastjunkies/gotball.mp3");
	AddFileToDownloadsTable("sound/knastjunkies/gotball.mp3");

	PrecacheModel("models/knastjunkies/soccerball.mdl");
	AddFileToDownloadsTable("models/knastjunkies/soccerball.mdl");
	AddFileToDownloadsTable("models/knastjunkies/SoccerBall.dx90.vtx");
	AddFileToDownloadsTable("models/knastjunkies/SoccerBall.phy");
	AddFileToDownloadsTable("models/knastjunkies/soccerball.vvd");

	AddFileToDownloadsTable("materials/knastjunkies/Material__0.vmt");
	AddFileToDownloadsTable("materials/knastjunkies/Material__1.vmt");

	//LoadBall();
}

public Action Event_SoundPlayed(clients[64], &numClients, String:sample[PLATFORM_MAX_PATH], &entity, &channel, &Float:volume, &level, &pitch, &flags)
{
	if(g_Ball == entity && StrEqual(sample, "~)weapons/hegrenade/he_bounce-1.wav")) {
		EmitSoundToAllAny("knastjunkies/bounce.mp3", entity);
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public OnClientDisconnect(int client)
{
	if (IsClientInGame(client))
		if (client == g_BallHolder) {
			g_BallHolder = 0;
			StartRespawnTimer();
		}
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{	
	static int g_fLastButtons[MAXPLAYERS+1];

	if(client == g_BallHolder) 
		if (g_fLastButtons[client] & IN_USE  && !(buttons & IN_USE)) {
			float fPower;
			fPower = GetGameTime() - g_fUseHoldTime[client];
			if (fPower > 0.5) fPower = 0.5;
			else if (fPower < 0.1) fPower = 0.1;
			KickBall(client, BALL_KICK_POWER * fPower * 18.0);
		} else {
			SetBallInFront(client);
			if (!(g_fLastButtons[client] & IN_USE) && buttons & IN_USE)
				g_fUseHoldTime[client] = GetGameTime();
		}
	g_fLastButtons[client] = buttons;
}

public EventPlayerDeath(Handle event, char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(client == g_BallHolder) {
		g_BallHolder = 0;
		StartRespawnTimer();
	}
}

public Action EventRoundStart(Handle event, char[] name, bool dontBroadcast) 
{
	dbgMsg("[SM] EventRoundStart------------------------------------------------------");
	if(g_BallSpawnExists)
		RespawnBall();

	StopRespawnTimer();
}

void StartRespawnTimer()
{
	if(g_TimerRespawn == INVALID_HANDLE)
		g_TimerRespawn = CreateTimer(BALL_AUTO_RESPAWN, TimerRespawnBall, _, TIMER_FLAG_NO_MAPCHANGE);
}

void StopRespawnTimer()
{
	if(g_TimerRespawn != INVALID_HANDLE)
		KillTimer(g_TimerRespawn);
	
	g_TimerRespawn = INVALID_HANDLE;
}

public Action TimerRespawnBall(Handle h)
{
	StopRespawnTimer();
	RespawnBall();
}

void OnBallKicked()
{
	g_BallHolder = 0;
	StartRespawnTimer();
}

void SetBallHolder(int client)
{
	if (client != g_BallHolder) {
		if(IsClientValid(g_BallHolder))
			SDKUnhook(g_BallHolder, SDKHook_TraceAttack, TraceAttack);
		
		g_BallHolder = client;
		
		SDKHook(client, SDKHook_TraceAttack, TraceAttack);

		float v[3];
		GetClientAbsOrigin(client, v);
		
		EmitAmbientSoundAny("knastjunkies/gotball.mp3", v);
		
		StopRespawnTimer();
	}
}

public Action TraceAttack(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &ammotype, int hitbox, int hitgroup) 
{ 
	if(victim == g_BallHolder && IsClientValid(attacker) && IsClientValid(victim) && victim != attacker)
		KickBall(victim, 500.0);
} 

void RecreateBall()
{
	Kill_g_iBall();
	CreateBall();
}

void CreateBall()
{
	g_Ball = CreateEntityByName("hegrenade_projectile");
	DispatchKeyValue(g_Ball, "targetname", BALL_ENTITY_NAME);
	
	DispatchSpawn(g_Ball);
	SetEntityModel(g_Ball, "models/knastjunkies/soccerball.mdl");

	SetEntProp(g_Ball, Prop_Send, "m_usSolidFlags", FSOLID_NOT_SOLID | FSOLID_TRIGGER);
	SetEntPropFloat(g_Ball, Prop_Data, "m_flModelScale", 0.60);
	
	Entity_SetMinSize(g_Ball, Float:{-BALL_RADIUS, -BALL_RADIUS, -BALL_RADIUS});
	Entity_SetMaxSize(g_Ball, Float:{BALL_RADIUS, BALL_RADIUS, BALL_RADIUS});
	
	SetEntityGravity(g_Ball, 0.8);
	
	SDKHook(g_Ball, SDKHook_Touch, OnBallTouch);
}

public OnBallTouch(int ball, int entity)
{
	if(g_BallHolder == 0 && IsClientValid(entity) && IsPlayerAlive(entity))
		SetBallHolder(entity);
}

void Kill_g_iBall()
{
	if(g_Ball && IsValidEntity(g_Ball))
		AcceptEntityInput(g_Ball, "Kill");
}

public bool BallTraceFilter(int entity, int mask, any client)
{
	return !IsClientValid(entity) && entity != g_Ball;
}

void RespawnBall()
{
	ClearBall();
	SetEntityMoveType(g_Ball, MOVETYPE_FLYGRAVITY);
	TeleportEntity(g_Ball, g_fBallSpawnOrigin, NULL_VECTOR, Float:{0.0, 0.0, 100.0});
}

void ClearBall()
{
	RecreateBall();
	g_BallHolder = 0;
}

void KickBall(int client, float power)
{
	if(IsInterferenceForKick(client, BALL_KICK_DISTANCE))
		return;
	
	float clientEyeAngles[3];
	GetClientEyeAngles(client, clientEyeAngles)	;
	
	float angleVectors[3];
	GetAngleVectors(clientEyeAngles, angleVectors, NULL_VECTOR, NULL_VECTOR);
	
	float ballVelocity[3];
	ballVelocity[0] = angleVectors[0] * power;
	ballVelocity[1] = angleVectors[1] * power;
	ballVelocity[2] = angleVectors[2] * power;
	
	float frontOrigin[3];
	GetClientFrontBallOrigin(client, BALL_KICK_DISTANCE, BALL_HOLD_HEIGHT + BALL_KICK_HEIGHT_ADDITION, frontOrigin);
	
	float kickOrigin[3];
	kickOrigin[0] = frontOrigin[0];
	kickOrigin[1] = frontOrigin[1];
	kickOrigin[2] = frontOrigin[2] + BALL_KICK_HEIGHT_ADDITION;

	RecreateBall();

	TeleportEntity(g_Ball, kickOrigin, NULL_VECTOR, ballVelocity)	;

	g_BallHolder = 0;
	
	OnBallKicked();
}

bool IsInterferenceForKick(int client, float kickDistance)
{
	float clientOrigin[3];
	GetClientAbsOrigin(client, clientOrigin);
	
	float clientEyeAngles[3];
	GetClientEyeAngles(client, clientEyeAngles);
		
	float cos = Cosine(DegToRad(clientEyeAngles[1]));
	float sin = Sine(DegToRad(clientEyeAngles[1]));
	
	float leftBottomOrigin[3];
	leftBottomOrigin[0] = clientOrigin[0] - sin * BALL_RADIUS;
	leftBottomOrigin[1] = clientOrigin[1] - cos * BALL_RADIUS;
	leftBottomOrigin[2] = clientOrigin[2] + BALL_HOLD_HEIGHT + BALL_KICK_HEIGHT_ADDITION - BALL_RADIUS;
	
	float startOriginAddtitions[3];
	startOriginAddtitions[0] = sin * BALL_RADIUS;
	startOriginAddtitions[1] = cos * BALL_RADIUS;
	startOriginAddtitions[2] = BALL_RADIUS;
	
	float testOriginAdditions[3];
	testOriginAdditions[0] = cos * (kickDistance + BALL_RADIUS);
	testOriginAdditions[1] = sin * (kickDistance + BALL_RADIUS);
	testOriginAdditions[2] = 0.0;	
	
	float startOrigin[3];
	float testOrigin[3];
	
	for(int x=0; x<3; x++)
		for(int y=0; y<3; y++)
			for(int z=0; z<3; z++) {
				startOrigin[0] = leftBottomOrigin[0] + x * startOriginAddtitions[0];
				startOrigin[1] = leftBottomOrigin[1] + y * startOriginAddtitions[1];
				startOrigin[2] = leftBottomOrigin[2] + z * startOriginAddtitions[2];
				
				for (int j=0; j<3; j++)
					testOrigin[j] = startOrigin[j] + testOriginAdditions[j];
				
				TR_TraceRayFilter(startOrigin, testOrigin, MASK_SOLID, RayType_EndPoint, BallTraceFilter, client);
				
				if(TR_DidHit())
					return true;
			}
	
	return false;
}

void SetBallInFront(int client)
{
	float origin[3];
	GetClientFrontBallOrigin(client, BALL_PLAYER_DISTANCE, BALL_HOLD_HEIGHT, origin);
	
	if (IsValidEntity(g_Ball))
		TeleportEntity(g_Ball, origin, NULL_VECTOR, Float:{0.0, 0.0, 100.0});
	else {
		LogError("SetBallInFront g_Ball is invalid ent");
		dbgMsg("SetBallInFront g_Ball is invalid ent");
		g_BallHolder = 0;
		StartRespawnTimer();
		Kill_g_iBall();
	}
}

void GetClientFrontBallOrigin(int client, float distance, int height, float destOrigin[3])
{
	float clientOrigin[3];
	GetClientAbsOrigin(client, clientOrigin);
	
	float clientEyeAngles[3];
	GetClientEyeAngles(client, clientEyeAngles);
	
	float cos = Cosine(DegToRad(clientEyeAngles[1]));
	float sin = Sine(DegToRad(clientEyeAngles[1]));
	
	destOrigin[0] = clientOrigin[0] + cos * distance;
	destOrigin[1] = clientOrigin[1] + sin * distance;
	destOrigin[2] = clientOrigin[2] + height;
}

//void LoadBall() 
//{
//	//char szPathConfig[PLATFORM_MAX_PATH];
//	//BuildPath(Path_SM, szPathConfig, sizeof szPathConfig, BALL_CFG_FILE);
//	
//	//Handle ConfigTree = CreateKeyValues("Spawns");
//	//FileToKeyValues(ConfigTree, szPathConfig);
//
//	//char szMap[50];
//	//GetCurrentMap(szMap, sizeof szMap);
//	
//	//if(KvJumpToKey(ConfigTree, szMap))  {
//		g_fBallSpawnOrigin[0] = KvGetFloat(ConfigTree, "x");
//		g_fBallSpawnOrigin[1] = KvGetFloat(ConfigTree, "y");
//		g_fBallSpawnOrigin[2] = KvGetFloat(ConfigTree, "z");
//		
//		g_BallSpawnExists = true;
//		CreateBall();
//		RespawnBall();
//	//}
//	
//	//CloseHandle(ConfigTree);
//}

stock Entity_SetMinSize(entity, float vecMins[3])
{
	SetEntPropVector(entity, Prop_Send, "m_vecMins", vecMins);
}

stock Entity_SetMaxSize(entity, float vecMaxs[3])
{
	SetEntPropVector(entity, Prop_Send, "m_vecMaxs", vecMaxs);
}

stock GetPlayerEyeViewPoint(iClient, float fPosition[3])
{
	float fAngles[3];
	GetClientEyeAngles(iClient, fAngles);

	float fOrigin[3];
	GetClientEyePosition(iClient, fOrigin);

	Handle hTrace = TR_TraceRayFilterEx(fOrigin, fAngles, MASK_SHOT, RayType_Infinite, TraceEntityFilterPlayer);
	
	if(TR_DidHit(hTrace)) {
		TR_GetEndPosition(fPosition, hTrace);
		CloseHandle(hTrace);

		return true;
	}
	
	CloseHandle(hTrace);
	
	return false;
}

public bool TraceEntityFilterPlayer(int iEntity, int iContentsMask)
{
	return iEntity > GetMaxClients();
}